const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const bodyParser = require("body-parser");
app.use(bodyParser.json());

const users = [{ name: "111", password: "222" }];

app.get("/", (req, res) => {
  res.json([123, 333]);
});

app.post("/login", (req, res) => {
  const findUser = users.find(
    ({ name, password }) =>
      name === req.body.name && password === req.body.password
  );

  if (!findUser) {
    res.status(401).json({ message: "name or password don't match" });
  }

  res.json({ token: "ouiiyu1kv3241414" });
});

server.listen(9999, () => {
  console.log("listening on *:9999");
});
