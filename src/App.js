import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./pages/Login/Login";
import Home from "./pages/Home/Home";
import Users from "./pages/Users/Users";
import Clients from "./pages/Clients/Clients";
import Client from "./pages/Client/Client";
import { URLS } from "./utils/consts";
import "./App.css";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path={URLS.login}>
          <Login />
        </Route>
        <Route path={URLS.users}>
          <Users />
        </Route>
        <Route path={URLS.clients}>
          <Clients />
        </Route>
        <Route path={`${URLS.client}/:id`}>
          <Client />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}
