export const URLS = {
  login: "/login",
  users: "/users",
  clients: "/clients",
  client: "/client",
};
