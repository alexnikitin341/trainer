import { createSlice } from "@reduxjs/toolkit";

const mockPersonalData = {
  surname: "Никитин",
  name: "Александр",
  patronymic: "Владиславович",
  birthDate: new Date(),
  phone: "89198924142",
  targeVisit: "улчшить тело",
  diseases: ["колено", "сердце"],
};

const anthropometryData = {
  weight: [
    {
      date: new Date(),
      value: 75,
    },
  ],
  height: [
    {
      date: new Date(),
      value: 173,
    },
  ],
  caliper: [
    {
      date: new Date(),
      aboveBreast: 10,
      underBreast: 10,
      navel: 29,
      iliac: 17,
      thigh: 12,
      triceps: 9,
      scapula: 14,
    },
  ],
};

const mockClient = {
  id: "1",
  ...mockPersonalData,
  ...anthropometryData,
};

export const clientsSlice = createSlice({
  name: "clients",
  initialState: {
    clients: [mockClient],
    choosedClient: null,
  },
  reducers: {
    addUser: (state, action) => {
      state.clients.push(action.payload);
    },

    deleteUser: (state, action) => {
      state.clients = state.clients.filter(({ id }) => id !== action.payload);
    },

    chooseClient: (state, action) => {
      state.choosedClient = state.clients.find(
        ({ id }) => id === action.payload
      );
    },
  },
});

export const { addUser, deleteUser, chooseClient } = clientsSlice.actions;

export const incrementAsync = (amount) => (dispatch) => {
  setTimeout(() => {
    dispatch(deleteUser("1"));
  }, 1000);
};

export const selectClients = (state) => state.clients.clients;
export const selectChoosedClient = (state) => state.clients.choosedClient;

export default clientsSlice.reducer;
