import { configureStore } from "@reduxjs/toolkit";
import clientsSlice from "./clientsSlice";

export default configureStore({
  reducer: {
    clients: clientsSlice,
  },
});
