import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { chooseClient, selectClients } from "../../redux/clientsSlice";
import { URLS } from "../../utils/consts";
import styles from "./style.module.scss";

const Clients = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const clients = useSelector(selectClients);

  const onClickClient = (id) => {
    dispatch(chooseClient(id));
    history.push(`${URLS.client}/${id}`);
  };

  return (
    <div className={styles.container}>
      <h1>CLIENTS</h1>

      {clients.map(({ id, name }) => (
        <div key={id} onClick={() => onClickClient(id)}>
          {name}
        </div>
      ))}
    </div>
  );
};

export default Clients;
