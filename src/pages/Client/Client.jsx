import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { chooseClient, selectChoosedClient } from "../../redux/clientsSlice";
import styles from "./style.module.scss";

const Client = () => {
  const client = useSelector(selectChoosedClient);
  let { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!client) {
      dispatch(chooseClient(id));
    }
  }, [client]);

  return (
    <div className={styles.container}>
      <h2>{client?.name}</h2>
    </div>
  );
};

export default Client;
