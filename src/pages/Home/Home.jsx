import { Link } from "react-router-dom";
import styles from "./style.module.scss";

const Home = () => {
  return (
    <div className={styles.container}>
      Home
      <Link to="/clients">Clients</Link>
    </div>
  );
};

export default Home;
